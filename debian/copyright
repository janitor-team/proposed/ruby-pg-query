Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pg_query
Source: http://github.com/lfittl/pg_query

Files: *
Copyright: 2014-2015, pganalyze Team <team@pganalyze.com>
           2008-2015, PostgreSQL Global Development Group
License: BSD-3-clause

Files: debian/*
Copyright: 2021 Pirate Praveen <praveen@debian.org>
License: BSD-3-clause
Comment: The Debian packaging is licensed under the same terms as the source.

Files: ext/pg_query/*
Copyright: 2015, 2017, Lukas Fittl <lukas@fittl.com>
           1996-2017, The PostgreSQL Global Development Group
           1994, The Regents of the University of California
License: BSD-3-clause and PostgreSQL

Files: ext/pg_query/src_port_qsort.c
Copyright: 1992, 1993, The Regents of the University of California
           J. L. Bentley and M. D. McIlroy
License: BSD-3-clause and PostgreSQL

Files: ext/pg_query/src_port_erand48.c
Copyright: 1996-2018, PostgreSQL Global Development Group
           1993 Martin Birgmeier
License: erand48

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
   1. Redistributions of source code must retain the above copyright notice, this
      list of conditions and the following disclaimer.
 .
   2. Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
 .
   3. Neither the name of the author, nor the names of its contributors may be
      used to endorse or promote products derived from this software without
      specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: PostgreSQL
 Permission to use, copy, modify, and distribute this software and its
 documentation for any purpose, without fee, and without a written agreement is
 hereby granted, provided that the above copyright notice and this paragraph and
 the following two paragraphs appear in all copies.
 .
 IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR
 DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
 PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
 THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.
 .
 THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
 BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND
 THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE,
 SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.


License: erand48
 Portions Copyright (c) 1996-2018, PostgreSQL Global Development Group
 .
 Portions Copyright (c) 1993 Martin Birgmeier
 .
 All rights reserved.
 .
 You may redistribute unmodified or modified versions of this source
 code provided that the above copyright notice and this and the
 following conditions are retained.
 .
 This software is provided ``as is'', and comes with no warranties
 of any kind. I shall in no event be liable for anything that happens
 to anyone/anything when using this software.
